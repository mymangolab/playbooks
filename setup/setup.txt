# Display facts from all hosts 
    ansible all -m setup

# Display facts from all hosts and store them 
    ansible all -m setup --tree /tmp/facts

# Filter only facts regarding ansible Ipv4 addresses and output them.
    ansible all -m setup -a 'filter=ansible_all_ipv4_addresses'

# Filter only facts regarding node name and output them.
    ansible all -m setup -a 'filter=ansible_nodename'

# Filter only facts regarding os family and output them.
    ansible all -m setup -a 'filter=ansible_os_family'

